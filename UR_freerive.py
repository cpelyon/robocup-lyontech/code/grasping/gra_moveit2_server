import socket
import time

# Robot's IP address and port
HOST = '10.68.0.101'  # Replace with your robot's IP address
PORT = 30002  # Port for URScript


# URScript command to activate Freedrive mode
activate_freedrive_script = "freedrive_mode()\n"

# URScript command to deactivate Freedrive mode
deactivate_freedrive_script = "end_freedrive_mode()\n"

def send_script(script):
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((HOST, PORT))
            s.sendall(script.encode('utf-8'))
            response = s.recv(1024)  # Lire la réponse du robot
            print(f"Received: {response}")
    except Exception as e:
        print(f"Error: {e}")

# Activer le mode Freedrive
print("Activating Freedrive mode...")
send_script(activate_freedrive_script)

# Attendre un certain temps pour permettre la manipulation manuelle
input("Press Enter to deactivate Freedrive mode...")

# Désactiver le mode Freedrive
print("Deactivating Freedrive mode...")
send_script(deactivate_freedrive_script)