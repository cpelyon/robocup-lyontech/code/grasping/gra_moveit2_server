#!/bin/bash
# Script de démarrage pour lancer les commandes ROS2

UR_CONTROLLER_IP=10.68.0.101
DASHBOARD_PORT=29999




# Fonction pour envoyer une commande au Dashboard Server et récupérer la réponse
send_command_and_capture_response() {
  local command=$1
  local response
  response=$(echo -e "$command\nquit\n" | nc $UR_CONTROLLER_IP $DASHBOARD_PORT)
  echo "$response" | sed -n '2s/[\r\n]*$//p'
}
# Liste d'états ordonnés
states=("NO_CONTROLLER" "DISCONNECTED" "CONFIRM_SAFETY" "BOOTING" "POWER_OFF" "POWER_ON" "IDLE" "BACKDRIVE" "RUNNING")

# Fonction pour obtenir l'indice d'un état dans la liste
get_state_index() {
    local state=$1
    for i in "${!states[@]}"; do
        if [[ "${states[$i]}" == "$state" ]]; then
            echo $i
            return
        fi
    done
    echo -1
}

# Fonction pour extraire l'état réel de la réponse
extract_state() {
    local response=$1
    # Extraction de l'état en supprimant la partie "Robotmode: "
    local state=$(echo $response | sed 's/Robotmode: //')
    echo $state
}

# Fonction pour exécuter une commande et vérifier l'état jusqu'à ce qu'il devienne "OK"
check_and_execute_command() {
    local max_initial_state=$1
    local command_to_send=$2
    local min_final_state=$3

    # Vérifier l'état actuel
    response=$(send_command_and_capture_response "check status")
    current_state=$(extract_state "$response")
    current_state_index=$(get_state_index "$current_state")
    max_initial_state_index=$(get_state_index "$max_initial_state")
    min_final_state_index=$(get_state_index "$min_final_state")

    if [[ $current_state_index -le $max_initial_state_index ]]; then
        # Envoyer la commande
        command="$command_to_send"
        response=$(send_command_and_capture_response "$command")
        echo "Commande '$command'  -->  $response "

        # Vérifier l'état jusqu'à ce qu'il soit au minimum l'état désiré
        check_until_min_status "$command" "$min_final_state"
    else
        echo "L'état actuel ($current_state) est supérieur à l'état maximum attendu ($max_initial_state)."
    fi
}

# Fonction pour vérifier l'état jusqu'à ce qu'il soit au minimum l'état désiré
check_until_min_status() {
    local command=$1
    local min_final_state=$2
    min_final_state_index=$(get_state_index "$min_final_state")

    while true; do
        response=$(send_command_and_capture_response "robotmode")
        current_state=$(extract_state "$response")
        current_state_index=$(get_state_index "$current_state")
        echo "Vérification de l'état actuel : $current_state"
        
        if [[ $current_state_index -ge $min_final_state_index ]]; then
            echo "État minimum désiré atteint : $current_state"
            break
        fi

        # Optionnel : Pause avant la prochaine vérification (par exemple, 1 seconde)
        sleep 1
    done
}

sleep 0.5

check_and_execute_command "POWER_OFF" "power on" "POWER_ON"

sleep 0.5

check_and_execute_command "POWER_ON" "brake release" "IDLE"

sleep 0.5

check_and_execute_command "IDLE" "play" "RUNNING"


