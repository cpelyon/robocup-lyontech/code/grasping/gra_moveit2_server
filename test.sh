#!/bin/bash

# Define the IP address and port of the Universal Robot RTDE server
ROBOT_IP="10.68.0.101" # Replace with the actual IP address of your robot
ROBOT_PORT="30004"      # Default RTDE port, replace if different

#python3 -m venv /venv
#source /venv/bin/activate
#pip3 install ur_rtde

# Loop until the robot is on

python3 check_robot_state.py
