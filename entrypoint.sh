#!/bin/bash
# Script de démarrage pour lancer les commandes ROS2


source /root/.bashrc
#source /opt/ros/rolling/setup.bash
source /opt/ros/humble/setup.bash
source /ws/install/setup.bash

echo "ROBOT_IP is set to: $ROBOT_IP"

PORT=29999
DASHBOARD_PORT=$PORT
#UR_CONTROLLER_IP=10.68.0.101
UR_CONTROLLER_IP=$ROBOT_IP




# -------------------- FUNCTIONS --------------------

wait_for_controller() {
    while true; do
        response=$(echo -n "robotmode" | nc -w 1 $UR_CONTROLLER_IP $PORT)
        echo $response
        
        if [[ $response == *"Universal Robots Dashboard Server"* ]]; then
            echo "UR controller is fully started and running."
            break
        else
            echo "UR controller is not running."
        fi
        sleep 1
    done
}

# Fonction pour envoyer une commande au Dashboard Server et récupérer la réponse
send_command_and_capture_response() {
  local command=$1
  local response
  response=$(echo -e "$command\nquit\n" | nc $UR_CONTROLLER_IP $DASHBOARD_PORT)
  echo "$response" | sed -n '2s/[\r\n]*$//p'
}
# Liste d'états ordonnés
states=("NO_CONTROLLER" "DISCONNECTED" "CONFIRM_SAFETY" "BOOTING" "POWER_OFF" "POWER_ON" "IDLE" "BACKDRIVE" "RUNNING")

# Fonction pour obtenir l'indice d'un état dans la liste
get_state_index() {
    local state=$1
    for i in "${!states[@]}"; do
        if [[ "${states[$i]}" == "$state" ]]; then
            echo $i
            return
        fi
    done
    echo -1
}

# Fonction pour extraire l'état réel de la réponse
extract_state() {
    local response=$1
    # Extraction de l'état en supprimant la partie "Robotmode: "
    local state=$(echo $response | sed 's/Robotmode: //')
    echo $state
}

# Fonction pour exécuter une commande et vérifier l'état jusqu'à ce qu'il devienne "OK"
check_and_execute_command() {
    local max_initial_state=$1
    local command_to_send=$2
    local min_final_state=$3

    # Vérifier l'état actuel
    response=$(send_command_and_capture_response "check status")
    current_state=$(extract_state "$response")
    current_state_index=$(get_state_index "$current_state")
    max_initial_state_index=$(get_state_index "$max_initial_state")
    min_final_state_index=$(get_state_index "$min_final_state")

    if [[ $current_state_index -le $max_initial_state_index ]]; then
        # Envoyer la commande
        command="$command_to_send"
        response=$(send_command_and_capture_response "$command")
        echo "Commande '$command'  -->  $response "

        # Vérifier l'état jusqu'à ce qu'il soit au minimum l'état désiré
        check_until_min_status "$command" "$min_final_state"
    else
        echo "L'état actuel ($current_state) est supérieur à l'état maximum attendu ($max_initial_state)."
    fi
}

# Fonction pour vérifier l'état jusqu'à ce qu'il soit au minimum l'état désiré
check_until_min_status() {
    local command=$1
    local min_final_state=$2
    min_final_state_index=$(get_state_index "$min_final_state")

    while true; do
        response=$(send_command_and_capture_response "robotmode")
        current_state=$(extract_state "$response")
        current_state_index=$(get_state_index "$current_state")
        echo "Vérification de l'état actuel : $current_state"
        
        if [[ $current_state_index -ge $min_final_state_index ]]; then
            echo "État minimum désiré atteint : $current_state"
            break
        fi

        # Optionnel : Pause avant la prochaine vérification (par exemple, 1 seconde)
        sleep 1
    done
}


# Fonction pour surveiller les logs
wait_for_log() {
    local log_file=$1
    local log_message=$2
    echo "Waiting for log message: $log_message"
    while ! grep -q "$log_message" "$log_file"; do
        sleep 1
    done
    echo "Log message found: $log_message"

    # Service available
    # Ready to receive control commands

}



# -------------------- BOOT ARM STEPS PROGRAM --------------------

#Attendre que le contrôleur soit prêt
wait_for_controller

#Démarrer le bras
sleep 1
check_and_execute_command "POWER_OFF" "power on" "POWER_ON"
sleep 1
check_and_execute_command "POWER_ON" "brake release" "IDLE"


# Lancer la première commande ROS2
#ros2 launch ur_robot_driver ur_control.launch.py ur_type:=ur5e robot_ip:=$UR_CONTROLLER_IP use_tool_communication:=true tool_voltage:=24 tool_parity:=0 tool_baud_rate:=115200 tool_stop_bits:=1 tool_rx_idle_chars:=1.5 tool_tx_idle_chars:=3.5 tool_device_name:=/tmp/ttyUR launch_rviz:=false use_mock_hardware:=false initial_controller:=joint_trajectory_controller
ros2 launch ur_robot_driver ur_control.launch.py ur_type:=ur5e robot_ip:=$UR_CONTROLLER_IP use_tool_communication:=true tool_voltage:=24 tool_parity:=0 tool_baud_rate:=115200 tool_stop_bits:=1 tool_rx_idle_chars:=1.5 tool_tx_idle_chars:=3.5 tool_device_name:=/tmp/ttyUR launch_rviz:=false use_mock_hardware:=false initial_controller:=joint_trajectory_controller  > /tmp/ros_launch.log 2>&1 &

sleep 15

# Attendre que le driver ROS pour le controller UR soit prêt
#wait_for_log /tmp/ros_launch.log "Ready to receive control commands" 
#wait_for_log /tmp/ros_launch.log "Configured and activated force_torque_sensor_broadcaster"
wait_for_log /tmp/ros_launch.log "process has finished cleanly"
sleep 3

check_and_execute_command "IDLE" "play" "RUNNING"

sleep 3

# Lancer Moveit2
#LC_NUMERIC=en_US.UTF-8 ros2 launch ur_moveit_config ur_moveit.launch.py ur_type:=ur5e launch_rviz:=$LAUNCH_RVIZ use_mock_hardware:=false &
LC_NUMERIC=en_US.UTF-8 ros2 launch ur_moveit_config ur_moveit.launch.py ur_type:=ur5e launch_rviz:=$LAUNCH_RVIZ use_mock_hardware:=false moveit_joint_limits_file:=/joint_limits.yaml &
#LC_NUMERIC=en_US.UTF-8 ros2 launch ur5_rq2f85_description ur_moveit.launch.py ur_type:=ur5e launch_rviz:=$LAUNCH_RVIZ use_mock_hardware:=false 

ros2 run rvl_robotiq_driver robotiq_controller &

sleep 10

#play -n synth 0.1 sine 880 vol 0.2
sleep 0.3
#play -n synth 0.1 sine 880 vol 0.2

ros2 service call /robotiq/activate std_srvs/srv/Trigger {}

ros2 run pymoveit2 arm_postapi.py



/bin/bash

# sleep 10

# quat=[-0.685496030838191, 0.14711056982266674, 0.7021394159825175, 0.11019899426933415]
# pos1=[-0.132, -0.692, 0.139]
# pos1=[-0.132, -0.692, 0.13]



# ros2 run pymoveit2 ex_pose_goal.py --ros-args -p position:="[0.25, 0.2, 0.75]" -p quat_xyzw:="$quat"
# sleep 3

# ros2 run pymoveit2 ex_pose_goal.py --ros-args -p position:="[0.25, 0.2, 0.70]" -p quat_xyzw:="$quat"
# sleep 3

# ros2 run pymoveit2 ex_pose_goal.py --ros-args -p position:="[0.25, 0.2, 0.75]" -p quat_xyzw:="$quat"
# sleep 3

# ros2 run pymoveit2 ex_pose_goal.py --ros-args -p position:="[0.25, 0.2, 0.70]" -p quat_xyzw:="$quat"

# docker exec -it ur5_moveit2 /bin/bash -c ". /opt/ros/humble/setup.bash && source /ws/install/setup.bash && ros2 run pymoveit2 ex_pose_goal.py"

## Bras rangé
# shoulder_pan_joint  = -1.5708
# shoulder_lift_joint = -1.5708
# elbow_joint         = -2.827
# wrist_1_joint       = 1.3521
# wrist_2_joint       = 0.0 
# wrist_3_joint       = 0.0



## Bras sortit
# shoulder_pan_joint  = -2.0
# shoulder_lift_joint = -1.0
# elbow_joint         = 0.6
# wrist_1_joint       = 1.7
# wrist_2_joint       = 0.9
# wrist_3_joint       = -1.6