FROM moveit/moveit2:humble-source

# Installer des dépendances supplémentaires
RUN apt-get update && \
    apt-get install -y \
    ros-humble-ur -y\
    ros-humble-rmw-cyclonedds-cpp -y\
    iproute2 -y\
    iputils-ping -y\
    software-properties-common -y \
    && rm -rf /var/lib/apt/lists/*

# RUN apt-get update && apt-get -y dist-upgrade
#RUN apt-get -y install  libc6
# RUN apt-get -y install python3-numpy python3-scipy 
#RUN apt install -y netcat-traditional 
RUN apt update && apt install -y netcat && which nc


RUN apt update && apt-get install -y python3-pymodbus python3-serial ros-humble-robotiq-* ros-humble-gripper-controllers

RUN apt update && apt-get install -y ros-humble-ros2-control ros-humble-ros2-controllers

# RUN apt-get update && \
#     apt install python3.12-venv -y && \
#     python3 -m venv /venv && \
#     /venv/bin/pip3 install ur_rtde



# Configurer le workspace ROS2
RUN mkdir -p /ws/src

# Copier les fichiers nécessaires dans le conteneur (assurez-vous que votre dossier contient les fichiers nécessaires)
#COPY . /ws/src/

SHELL ["/bin/bash", "-c"]

# Copier les fichiers nécessaires dans le conteneur
#COPY . /ws/src/

# Configuration de l'environnement
ENV ROS_DOMAIN_ID=0
# Controlleur robot : 10.68.0.101
# Simulateur : 172.17.0.2
ENV ROBOT_IP=172.17.0.2
ENV LAUNCH_RVIZ=false

# Expose the necessary ports for ROS communication
EXPOSE 11311 5900 5901 5902 5903 5904 5905 5906 5907 5908 5909 5910

# Construire le workspace
RUN source /opt/ros/humble/setup.bash && \
    cd /ws/src && \
    git clone https://github.com/robotic-vision-lab/UR-Robotiq-Integrated-Driver.git && \
    git clone https://github.com/UniversalRobots/Universal_Robots_ROS2_Description.git -b humble && \
    mv /ws/src/UR-Robotiq-Integrated-Driver/colcon_ws/src/rvl_robotiq_msgs . && \
    mv /ws/src/UR-Robotiq-Integrated-Driver/colcon_ws/src/rvl_robotiq_driver . && \
    rm -rf /ws/src/UR-Robotiq-Integrated-Driver && \
    cd /ws && \
    rosdep install -y -r -i --rosdistro ${ROS_DISTRO} --from-paths . && \
    colcon build --base-paths /ws/src --merge-install --symlink-install --cmake-args "-DCMAKE_BUILD_TYPE=Release"


RUN apt-get update && \
    apt install python3-venv python3-pip sox libsox-fmt-all -y && \
    /bin/python3 -m pip install fastapi uvicorn


RUN source /opt/ros/humble/setup.bash && \
    cd /ws/src && \
    git clone https://github.com/tylerjw/serial.git -b ros2 && \  
    cd /ws && \
    rosdep install -y -r -i --rosdistro ${ROS_DISTRO} --from-paths . && \
    colcon build --base-paths /ws/src --merge-install --symlink-install --cmake-args "-DCMAKE_BUILD_TYPE=Release"    


RUN source /opt/ros/humble/setup.bash && \
    cd /ws/src && \
    git clone https://github.com/m0rph03nix/pymoveit2.git  -b               test_server && \  
    git clone https://github.com/PickNikRobotics/ros2_robotiq_gripper.git && \
    git clone https://gitlab.com/cpelyon/robocup-lyontech/code/grasping/gra_arm_gripper_moveit_config.git        --branch               forward_v0 && \    
    cd /ws && \
    rosdep install -y -r -i --rosdistro ${ROS_DISTRO} --from-paths . && \
    colcon build --base-paths /ws/src --merge-install --symlink-install --cmake-args "-DCMAKE_BUILD_TYPE=Release"  


    # Ajouter les commandes d'initialisation de l'environnement ROS au .bashrc
RUN echo "source /opt/ros/humble/setup.bash" >> /root/.bashrc && \
    echo "source /ws/install/setup.bash" >> /root/.bashrc && \
    echo "export RMW_IMPLEMENTATION=rmw_cyclonedds_cpp" >> /root/.bashrc


# Copier le script d'entrée
COPY entrypoint.sh /entrypoint.sh
COPY joint_limits.yaml /joint_limits.yaml
COPY check_robot_state.py /check_robot_state.py
RUN chmod +x /entrypoint.sh
RUN chmod +x /check_robot_state.py

# Définir le script comme point d'entrée
ENTRYPOINT ["/entrypoint.sh"]
#CMD ["/bin/bash"]

# Définir les commandes de lancement
#CMD ["/bin/bash", "-c", "\
#  ros2 launch ur_robot_driver ur_control.launch.py ur_type:=ur5e robot_ip:=$UR_CONTROLLER_IP use_tool_communication:=true tool_voltage:=24 tool_parity:=0 tool_baud_rate:=115200 tool_stop_bits:=1 tool_rx_idle_chars:=1.5 tool_tx_idle_chars:=3.5 tool_device_name:=/tmp/ttyUR launch_rviz:=false use_mock_hardware:=false initial_controller:=joint_trajectory_controller && \
#  LC_NUMERIC=en_US.UTF-8 ros2 launch ur_moveit_config ur_moveit.launch.py ur_type:=ur5e launch_rviz:=$LAUNCH_RVIZ use_mock_hardware:=false \
#"]