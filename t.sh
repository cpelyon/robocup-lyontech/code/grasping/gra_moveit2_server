#!/bin/bash
ROBOT_IP="10.68.0.101"
PORT=29999


while true; do
    response=$(echo -n "robotmode" | nc -w 1 $ROBOT_IP $PORT)
    echo $response
    
    if [[ $response == *"Universal Robots Dashboard Server"* ]]; then
        echo "UR controller is fully started and running."
        break
    else
        echo "UR controller is not running."
    fi

    sleep 1

done
