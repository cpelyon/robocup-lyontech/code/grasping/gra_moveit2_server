#!/bin/bash
# Script de démarrage pour lancer les commandes ROS2


source /root/.bashrc
#source /opt/ros/rolling/setup.bash
source /opt/ros/humble/setup.bash
source /ws/install/setup.bash


PORT=29999
DASHBOARD_PORT=$PORT
#UR_CONTROLLER_IP=10.68.0.101
#UR_CONTROLLER_IP=127.0.1.1

# Default IP
DEFAULT_UR_CONTROLLER_IP="10.68.0.101"

# Use the provided argument or the default value
UR_CONTROLLER_IP=${1:-$DEFAULT_UR_CONTROLLER_IP}


# -------------------- FUNCTIONS --------------------

wait_for_controller() {
    while true; do
        response=$(echo -n "robotmode" | nc -w 1 $UR_CONTROLLER_IP $PORT)
        echo $response
        
        if [[ $response == *"Universal Robots Dashboard Server"* ]]; then
            echo "UR controller is fully started and running."
            break
        else
            echo "UR controller is not running."
        fi
        sleep 1
    done
}

# Fonction pour envoyer une commande au Dashboard Server et récupérer la réponse
send_command_and_capture_response() {
  local command=$1
  local response
  response=$(echo -e "$command\nquit\n" | nc $UR_CONTROLLER_IP $DASHBOARD_PORT)
  echo "$response" | sed -n '2s/[\r\n]*$//p'
}
# Liste d'états ordonnés
states=("NO_CONTROLLER" "DISCONNECTED" "CONFIRM_SAFETY" "BOOTING" "POWER_OFF" "POWER_ON" "IDLE" "BACKDRIVE" "RUNNING")

# Fonction pour obtenir l'indice d'un état dans la liste
get_state_index() {
    local state=$1
    for i in "${!states[@]}"; do
        if [[ "${states[$i]}" == "$state" ]]; then
            echo $i
            return
        fi
    done
    echo -1
}

# Fonction pour extraire l'état réel de la réponse
extract_state() {
    local response=$1
    # Extraction de l'état en supprimant la partie "Robotmode: "
    local state=$(echo $response | sed 's/Robotmode: //')
    echo $state
}

# Fonction pour exécuter une commande et vérifier l'état jusqu'à ce qu'il devienne "OK"
check_and_execute_command() {
    local max_initial_state=$1
    local command_to_send=$2
    local min_final_state=$3

    # Vérifier l'état actuel
    response=$(send_command_and_capture_response "check status")
    current_state=$(extract_state "$response")
    current_state_index=$(get_state_index "$current_state")
    max_initial_state_index=$(get_state_index "$max_initial_state")
    min_final_state_index=$(get_state_index "$min_final_state")

    if [[ $current_state_index -le $max_initial_state_index ]]; then
        # Envoyer la commande
        command="$command_to_send"
        response=$(send_command_and_capture_response "$command")
        echo "Commande '$command'  -->  $response "

        # Vérifier l'état jusqu'à ce qu'il soit au minimum l'état désiré
        check_until_min_status "$command" "$min_final_state"
    else
        echo "L'état actuel ($current_state) est supérieur à l'état maximum attendu ($max_initial_state)."
    fi
}

# Fonction pour vérifier l'état jusqu'à ce qu'il soit au minimum l'état désiré
check_until_min_status() {
    local command=$1
    local min_final_state=$2
    min_final_state_index=$(get_state_index "$min_final_state")

    while true; do
        response=$(send_command_and_capture_response "robotmode")
        current_state=$(extract_state "$response")
        current_state_index=$(get_state_index "$current_state")
        echo "Vérification de l'état actuel : $current_state"
        
        if [[ $current_state_index -ge $min_final_state_index ]]; then
            echo "État minimum désiré atteint : $current_state"
            break
        fi

        # Optionnel : Pause avant la prochaine vérification (par exemple, 1 seconde)
        sleep 1
    done
}


# Fonction pour surveiller les logs
wait_for_log() {
    local log_file=$1
    local log_message=$2
    echo "Waiting for log message: $log_message"
    while ! grep -q "$log_message" "$log_file"; do
        sleep 1
    done
    echo "Log message found: $log_message"

    # Service available
    # Ready to receive control commands

}



# -------------------- BOOT ARM STEPS PROGRAM --------------------

# Attendre que le contrôleur soit prêt
#wait_for_controller

# Démarrer le bras
#sleep 0.5
#check_and_execute_command "POWER_OFF" "power on" "POWER_ON"
commands=("power off" "power on" "play" "pause" "stop" "shutdown" "brake release" "unlock protective stop" "restart safety" "close popup" )
echo  "power off  -  power on  -  play  -  pause  -  stop  -  shutdown  -  brake release  -  unlock protective stop  -  restart safety  -  close popup "
#check_and_execute_command "DISCONNECTED" "power off" "POWER_OFF"

# # Liste des modes disponibles et leur feedback associé
# modes=("NO_CONTROLLER" "DISCONNECTED" "CONFIRM_SAFETY" "BOOTING" "POWER_OFF" "POWER_ON" "IDLE" "BACKDRIVE" "RUNNING")

# Initialisation du mode actuel
current_mode=0


# Boucle pour écouter les entrées clavier
while true; do

    # Liste des commandes
    commands=("power off" "power on" "play" "stop" "brake release" "unlock protective stop" "restart safety" "close popup" "load freedrive.urp")

    # Afficher les commandes avec leurs indices
    for i in "${!commands[@]}"; do
        echo "$((i+1)). ${commands[$i]}"
    done




    # Lire une seule touche sans attendre (pas de newline nécessaire)
    read -rsn1 mode_key

    # Vérifier la touche pressée et changer de mode si c'est une touche numérique
    case $mode_key in
        1) current_mode=0 ;;
        2) current_mode=1 ;;
        3) current_mode=2 ;;
        4) current_mode=3 ;;
        5) current_mode=4 ;;
        6) current_mode=5 ;;
        7) current_mode=6 ;;
        8) current_mode=7 ;;
        9) current_mode=8 ;;
        q) echo "Sortie du programme." && exit ;;
        *) echo "Touche non reconnue. Appuyez sur 1, 2, 3 pour changer de mode, ou q pour quitter." ;;
    esac

    # Effacer la ligne du feedback précédent
    #tput cuu1
    #tput el


    command=${commands[$current_mode]}
    response=$(send_command_and_capture_response "$command")
    echo "Commande '$command'  -->  $response "

    # Vérifier l'état jusqu'à ce qu'il soit au minimum l'état désiré
    check_until_min_status "$command" "$min_final_state"    
done