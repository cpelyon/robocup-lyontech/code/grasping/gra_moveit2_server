#!/bin/bash

# Lancer les services
docker-compose -f docker-compose-sim.yml up

# Nettoyer les conteneurs arrêtés
docker-compose down --rmi local --volumes --remove-orphans