# LyonTech Grasping MoveIt2 Server


## Run container with simulator

export UR_CONTROLLER_IP=172.17.0.2
export LAUNCH_RVIZ=true
docker-compose -f docker-compose-sim.yml up

## Run container manually

- Once for all, build the docker image
    ```shell
    docker build -t lyontech_grasping/ur5_moveit2:v1 . 
    ```  


- Run container
    ```bash
    xhost +
    # UR_CONTROLLER_IP à changer en fonction du robot
    docker run -it  -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix --env ROBOT_IP=10.68.0.101 --env LAUNCH_RVIZ=true --net=host --name=ur5_moveit2 lyontech_grasping/ur5_moveit2:v1
    ```

- If needed, execute other bash access to the container
    ```bash
    xhost +
    docker exec -it ur5_moveit2 /bin/bash
    ```

## Tools

 - Outil pour changer l'état du controller du bras

    ```bash
    ./UR_modes.sh
    ```



- Bras rangé
    ```bash
    curl -X POST "http://127.0.1.1:8000/move" -H "Content-Type: application/json" -d '{
        "joint_positions": [-1.57, -1.39, -2.81, -2.14, -3.12, -1.0], 
        "synchronous": true,
        "cancel_after_secs": 0.0
    }'
    ```


- Bras wait bag
    ```bash
    curl -X POST "http://127.0.1.1:8000/move" -H "Content-Type: application/json" -d '{
        "joint_positions": [-1.571, -1.39, -2.374, -1.25, -1.57, -1.62],
        "synchronous": true,
        "cancel_after_secs": 0.0
    }'
    ```    

- Bras carry bag
    ```bash
    curl -X POST "http://127.0.1.1:8000/move" -H "Content-Type: application/json" -d '{
        "joint_positions": [-1.562, -1.38, -2.778, 0.13, -1.796, 0.087],
        "synchronous": true,
        "cancel_after_secs": 0.0
    }'
    ```   


- Bras point front
    ```bash
    curl -X POST "http://127.0.1.1:8000/move" -H "Content-Type: application/json" -d '{
        "joint_positions": [-1.57, -1.39, -2.6, -2.13, -1.5, -1.57],
        "synchronous": true,
        "cancel_after_secs": 0.0
    }'
    ```        