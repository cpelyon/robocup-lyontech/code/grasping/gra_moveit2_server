import rtde_receive, rtde_control
import sys
from time import sleep

# IP address of the Universal Robot
ROBOT_IP = "10.68.0.101"  # Replace with the actual IP address of your robot

def is_robot_on():
    cnt = 0
    while True:
        try:
            rtde_c = rtde_control.RTDEControlInterface(ROBOT_IP)
            rtde_r = rtde_receive.RTDEReceiveInterface(ROBOT_IP)
            robot_mode = rtde_r.getRobotMode()
            # Robot is considered ON if in modes 2, 3, 4 (IDLE, BACKDRIVE, RUNNING)
            print(f"Robot ready: {robot_mode in [2, 3, 4]}")
            if robot_mode in [2, 3, 4]:
                return True
        except Exception as e:
            print(f"Error: {e}")
            cnt = cnt +1
            if cnt > 10:
                return False
            
        sleep(1)

if __name__ == "__main__":
    if is_robot_on():
        sys.exit(0)
    else:
        sys.exit(1)